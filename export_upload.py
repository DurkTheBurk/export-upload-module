import bpy
import os
import shutil
import time
import requests
from threading import Thread


if bpy.app.version[1] >= 80:
    temp_path = bpy.context.preferences.filepaths.temporary_directory
else:
    temp_path = bpy.context.user_preferences.filepaths.temporary_directory

current_file = bpy.data.filepath


def make_temp():
    bpy.ops.wm.save_mainfile()
    name = '{}_{}_{}'.format('export', int(time.time()*1000), os.path.basename(current_file))
    shutil.copy(current_file, os.path.join(temp_path, name))
    return os.path.join(temp_path, name)


def delete_current_temp():
    for blend in os.listdir(temp_path):
        if 'export_' in blend:
            if os.path.basename(current_file) in blend:
                print('delete', blend)


def delete_all_temp():
    for blend in os.listdir(temp_path):
        if 'export_' in blend:
            print('Deleting', blend)
            os.remove(os.path.join(temp_path, blend))


def upload(server, temp_file, file_name):
    def thread():
        signed_url = requests.get(server, {'file_name': file_name}).text
        response = requests.put(signed_url, open(temp_file, 'rb'))
        delete_current_temp()
        return response
    Thread(target=thread).start()


def download(server, file_name):
    def thread():
        signed_url = requests.get(server, {'file_name': file_name}).text
        data = requests.get(signed_url).content
        with open(os.path.join(temp_path, file_name), 'wb') as f:
            f.write(data)
        return os.path.join(temp_path, file_name)
    Thread(target=thread).start()


#Upload

'''
server = 'http://127.0.0.1:7776/'
location = make_temp()
name = 'Blend_test.blend'
upload(server, location, name)
'''

#Download
'''
server = 'http://127.0.0.1:7776/download'
name = 'uuid_patricks_blend.blend'
download(server, name)
'''
